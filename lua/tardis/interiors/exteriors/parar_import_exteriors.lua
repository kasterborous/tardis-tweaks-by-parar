TARDIS:ImportExterior("tuat", { category = "Exteriors.Categories.PoliceBoxes", })
TARDIS:ImportExterior("ambient", { category = "Exteriors.Categories.PoliceBoxes", })
TARDIS:ImportExterior("jorj_whittaker", { category = "Exteriors.Categories.PoliceBoxes", })

TARDIS:ImportExterior("rani")
TARDIS:ImportExterior("timmy2985")
TARDIS:ImportExterior("gnome")
TARDIS:ImportExterior("Bill and Ted Tardis")

TARDIS:ImportExterior("72alt", { name = "Computer bank", })

TARDIS:ImportExterior("win-quartertardis", {
    category = "Exteriors.Categories.PoliceBoxes",
    modify_func = function(E)
        E.Portal.thickness = 27.6
        E.Portal.width = 28
        E.Portal.inverted = true
        return E
    end
})

TARDIS:ImportExterior("brun_interior", {
    ext_id = "brun",
    category = "Exteriors.Categories.PoliceBoxes",
    modify_func = function(E)
        E.Portal.pos = Vector(30, 0, 52.2)
        E.Portal.height = 89.4
        E.Portal.thickness = 27
        E.Portal.inverted = true

        E.Parts.door.posoffset = Vector(-30,0,-52.2)

        return E
    end
})


--[[
TODO:
Diamond
Diamond_v2
minibox
nani
nelvanatardis (x2?)

jorj_whittaker_master
roamingeye
copperturdis
]]